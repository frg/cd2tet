#ifndef _BIN_FILE_HANDLER_H_
#define _BIN_FILE_HANDLER_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define MAXLINE 500

//------------------------------------------------------------------------------

class BinFileHandler {

public:

  //typedef off_t OffType;
#ifdef sgi
  typedef long OffType; 
#else
#ifdef linux
  typedef long long OffType;
#else
#error Update the definition of OffType for your machine
#endif
#endif

private:

  OffType cpos;

  int headersize;

  double version;

  bool swapBytes;

  int fileid;

  FILE *file;

public:

  BinFileHandler(char *, char *, double = 0.0);
  ~BinFileHandler();

  template<class Scalar>
  void swapVector(Scalar *, int);

  template<class Scalar>
  void read(Scalar *, int);

  template<class Scalar>
  void write(Scalar *, int);

  void seek(OffType);

  OffType tell();

  double getVersion() const { return version; }

};

//------------------------------------------------------------------------------

template <class Scalar>
void BinFileHandler::read(Scalar *p, int nobjs)
{

  if (file) fread(p, sizeof(Scalar), nobjs, file);
  else ::read(fileid, p, nobjs*sizeof(Scalar));

  cpos += nobjs*sizeof(Scalar);

  if (swapBytes) swapVector(p, nobjs);

}

//------------------------------------------------------------------------------

template <class Scalar>
void BinFileHandler::write(Scalar *p, int nobjs)
{

  if (swapBytes) swapVector(p, nobjs);

  if (file) fwrite(p, sizeof(Scalar), nobjs, file);
  else ::write(fileid, p, nobjs*sizeof(Scalar));

  cpos += nobjs*sizeof(Scalar);

  if (swapBytes) swapVector(p, nobjs);

}

//------------------------------------------------------------------------------

template <class Scalar>
void BinFileHandler::swapVector(Scalar *p, int nobjs)
{

  for (int obj = 0; obj < nobjs; ++obj) {

    Scalar x = p[obj];

    char *px = (char *) &x;
    char *pp = (char *) (p+obj);

    for (int c = 0; c < sizeof(Scalar); ++c)
      pp[sizeof(Scalar)-1-c] = px[c];

  }

}


//------------------------------------------------------------------------------

inline
void BinFileHandler::seek(BinFileHandler::OffType size) 
{ 

  size += headersize;

  if (file) fseek(file, size, SEEK_SET);
  else lseek(fileid, size, SEEK_SET);

  cpos = size;

}


//------------------------------------------------------------------------------

inline
BinFileHandler::OffType BinFileHandler::tell() 
{ 

  int pos;

  if (file) 
    pos = ftell(file);
#ifdef sgi
  else 
    pos = ::tell(fileid);
#else
  else
    pos = cpos;
#endif

  pos -= headersize;

  return pos;

}

//------------------------------------------------------------------------------

inline
BinFileHandler::BinFileHandler(char *name, char *flag, double ver) :
  file(0), fileid(0), swapBytes(0), version(ver) 
{

  int ierr = 0;

  if (strcmp(flag, "r") == 0) {
    fileid = open(name, O_RDONLY, 0644);
    if (fileid == -1) ierr = 1;
  }
  else if (strcmp(flag, "w") == 0) {
    fileid = open(name, O_WRONLY | O_CREAT | O_TRUNC, 0644);
    if (fileid == -1) ierr = 1;
  }
  else if (strcmp(flag, "ws") == 0) {
    fileid = open(name, O_WRONLY | O_CREAT | O_TRUNC | O_SYNC, 0644);
    if (fileid == -1) ierr = 1;
  }
  else if (strcmp(flag, "w+") == 0) {
    fileid = open(name, O_WRONLY | O_CREAT, 0644);
    if (fileid == -1) ierr = 1;
  }
  else if (strcmp(flag, "ws+") == 0) {
    fileid = open(name, O_WRONLY | O_CREAT | O_SYNC, 0644);
    if (fileid == -1) ierr = 1;
  }
  else if (strcmp(flag, "rb") == 0) {
    file = fopen(name, "rb");
    if (!file) ierr = 1;
  }
  else if (strcmp(flag, "wb") == 0) {
    file = fopen(name, "wb");
    if (!file) ierr = 1;
  }
  else {
    fprintf(stderr, "*** Error: wrong flag (%s) for \'%s\'\n", flag, name);
    exit(1);
  }

  if (ierr) {
    fprintf(stderr, "*** Error: unable to open \'%s\'\n", name);
    exit(1);
  }

  headersize = sizeof(int) + sizeof(double);
    
  int one = 1;
  if (strcmp(flag, "r") == 0 || strcmp(flag, "rb") == 0) {
    read(&one, 1);
    if (one != 1) swapBytes = 1;
    read(&version, 1);
  } 
  else if (strcmp(flag, "w") == 0 || strcmp(flag, "ws") == 0 || strcmp(flag, "wb") == 0) {
    write(&one, 1);
    write(&version, 1);
  }
  else if (strcmp(flag, "w+") == 0 || strcmp(flag, "ws+") == 0)
    seek(0);

  cpos = headersize;

}

//------------------------------------------------------------------------------

inline
BinFileHandler::~BinFileHandler() 
{ 

  if (file) fclose(file); 
  else close(fileid);

}

//------------------------------------------------------------------------------

inline
int computeNumberOfDigits(int num)
{

  int digits = 1;

  while (num >= 10) {
    num /= 10;
    ++digits;
  }

  return digits;

}

//------------------------------------------------------------------------------

inline
char *computeClusterSuffix(int num, int maxNum)
{

  int numZeros = computeNumberOfDigits(maxNum) - computeNumberOfDigits(num);

  char zeros[100];
  char *suffix = new char[100];

  strcpy(zeros, "");
  for (int k=0; k<numZeros; ++k)
    strcat(zeros, "0");

  sprintf(suffix, "%s%d", zeros, num);

  return suffix;

}

//------------------------------------------------------------------------------

#endif
