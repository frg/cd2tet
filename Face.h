#ifndef _FACE_H_
#define _FACE_H_

#ifdef OLD_STL
#include <defalloc.h>
#include <algo.h>
#include <map.h>
#include <stack.h>
#else
#include <algorithm>
#include <map>
#include <stack>
using std::stable_sort;
using std::map;
using std::stack;
#endif

#ifdef __KCC
#include <utility.h>
#else
#include <utility>
using std::pair;
#endif

#include <stdio.h>
#include <BlockAlloc.h>

#ifdef OLD_STL
typedef map<pair<int,int>, pair<int,int>, less<pair<int,int> > > EdgeMap;
#else
typedef map<pair<int,int>, pair<int,int> > EdgeMap;
#endif

struct Vec3D;
template<class Scalar, int dim> class SVec;
class BFace;

//------------------------------------------------------------------------------

namespace FaceDefs {

  enum Type {TRIA=4};
  
  static const int MaxNumNd = 4;
  static const int MaxNumEd = 4;
  
}

//------------------------------------------------------------------------------

class Face {

public:

  Face() {}
  ~Face() {}

  virtual int  numNodes() const = 0;
  virtual int  numEdges() const = 0;
  virtual int* nodeNum() = 0;
  virtual int& nodeNum(int i) = 0;
  virtual int  nodeNum(int i) const = 0;
  virtual const int  edgeDef(int i, int k) = 0;
  virtual const bool lt(const Face &f) const = 0;
  virtual void  order() = 0;
  virtual void  swap() = 0;
  virtual const FaceDefs::Type ftype() = 0;

  int &operator[](int i) { return nodeNum(i); }  
  bool operator<(const Face &f) const {
    return lt(f);
  }

  virtual void printNodes(char *str) = 0;

  virtual BFace *copyFace() = 0;
  virtual BFace *copyFace(BlockAlloc &bfmem) = 0;

  virtual double distanceToFace(double pos[3], SVec<double,3>& X) = 0;

  virtual Vec3D computeNormal(SVec<double, 3>& X) = 0;

};

//------------------------------------------------------------------------------

class FaceTria : public virtual Face {

  static const int edge_def[3][2];

  int nodes[3];

public:

  FaceTria() {}
  ~FaceTria() {}

  int numNodes() const { return 3; }
  int numEdges() const { return 3; }
  int* nodeNum() { return nodes; }
  int& nodeNum(int i) { return nodes[i]; }
  int  nodeNum(int i) const { return nodes[i]; }
  const int edgeDef(int i, int k) { return edge_def[i][k]; }

  const FaceDefs::Type ftype() { return FaceDefs::TRIA; }

  void order() {
#ifdef OLD_STL
    sort(&nodes[0], &nodes[3]);
#else
    stable_sort(&nodes[0], &nodes[3]);
#endif
  }

  void swap() {
    int nold[3] = {nodes[0], nodes[1], nodes[2] };
    
    nodes[0] = nold[0];
    nodes[1] = nold[2];
    nodes[2] = nold[1];
  }

  const bool lt(const Face &f) const {
    if (numNodes()==f.numNodes())
      return 
	( nodeNum(0) < f.nodeNum(0) ) ||
	(!(f.nodeNum(0) < nodeNum(0)) && nodeNum(1) < f.nodeNum(1)) ||
	(!(f.nodeNum(0) < nodeNum(0)) && !(f.nodeNum(1) < nodeNum(1)) && nodeNum(2) < f.nodeNum(2));
    else
      return (numNodes() < f.numNodes());
  }

  BFace *copyFace();
  BFace *copyFace(BlockAlloc &bfmem);

  void printNodes(char *str) {
    sprintf(str, "%d %d %d", nodes[0]+1, nodes[1]+1, nodes[2]+1);
  }

  double distanceToFace(double pos[3], SVec<double,3>& X);

  static double compute_distance_pt_to_tri(double* p, double* a, double* b, double *c,
					   double* pfSParam = 0, double* pfTParam = 0);
  
  Vec3D computeNormal(SVec<double, 3>& X);
  
};

//------------------------------------------------------------------------------

class BFace : public virtual Face {

protected:

  int type;
  int color;
  int lelement;
  int surface_id;

public:

  BFace() { type = -1; color = -1; lelement = -1; surface_id = 0; }
  ~BFace() {}

  void set_type(int t) { type = t; }
  int  get_type() const { return type; }

  void set_color(int c) { color = c; }
  int  get_color() const { return color; }

  void set_lelement(int e) { lelement = e; }
  int  get_lelement() const { return lelement; }

  void build_edges(int, EdgeMap &);
  void count_node2faces(int, int*);
  void build_node2faces(int, int*, int**);
  void paint(int, int, int, int*, int**, stack<int>&);
  void setSurfaceID(int i) { surface_id = i; }
  int  getSurfaceID() { return surface_id; }

  virtual BFace *copyBFace() = 0;
  virtual BFace *copyBFace(BlockAlloc &bfmem) = 0;

};

//------------------------------------------------------------------------------

class BFaceTria : public BFace, public FaceTria {

public:
  BFaceTria(int n[3], int t = -1, int ln = -1, int sid = 0) { 
    type = t; color = -1; lelement = ln; 
    nodeNum(0) = n[0]; nodeNum(1) = n[1]; nodeNum(2) = n[2];
    surface_id = sid;
  }

  BFace *copyBFace() {
    return (new BFaceTria(nodeNum()));
  }

  BFace *copyBFace(BlockAlloc &bfmem) {
    return (new (bfmem) BFaceTria(nodeNum()));
  }

};

//------------------------------------------------------------------------------

class CmpFace {

public:

  bool operator() (const Face* f1, const Face* f2) const {
    return ((*f1)<(*f2));
  }

};

//------------------------------------------------------------------------------

typedef map<Face*, pair<int,int>, CmpFace > FaceMap;
typedef map<BFace*, pair<int,int>, CmpFace > BFaceMap;

//------------------------------------------------------------------------------

class BFaceSet {

  int nfaces;
  BFace** faces;
  BlockAlloc memFaces;
  
public:

  BFaceSet();
  ~BFaceSet();

  BFace& operator[](int i) const { return *faces[i]; }
  int size() { return nfaces; }

  // Add pointer to BFace at position i in faceset
  void addFace(int i, BFace *face) { faces[i] = face; }

  // Add pointer to BFace at position i in faceset
  BFace& createFace(int i, FaceDefs::Type ftype, int *n, int t = -1, int ln = -1, int sid = 0) {

    // create face in faceset depending on type
    switch (ftype) {
    case FaceDefs::TRIA:
      addFace(i, new(memFaces) BFaceTria(n, t, ln, sid));
      break;

    default:
      fprintf(stderr, "Error: Could not find face type %d\n", ftype);
      exit(1);
    }
    
    return *faces[i];
  }

  // Copy BFace at position i in face set
  void copyFace(int i, BFace *face) {

    // Allocate memory
    BFace &bf = createFace(i, face->ftype(), face->nodeNum());

    // Make copy
    bf = *face;

  }

  int countFaceType(const FaceDefs::Type type) {
    int count = 0;

    for (int i=0; i<nfaces; i++)
      if (faces[i]->ftype()==type)
	count++;
    
    return count;
  }

  void resize(int);

};

//------------------------------------------------------------------------------

#endif
