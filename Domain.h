#ifndef _DOMAIN_H_
#define _DOMAIN_H_

#include <Node.h>
#include <Face.h>
#include <Element.h>

#define MIN_TYPE_BC -6
#define MAX_TYPE_BC 10
#define MAXLINE 500

//------------------------------------------------------------------------------

class Domain {

  char* ressuffix;
  char** ori_sol_name;

protected:

  bool createIcem;
  bool check;
  bool compute_dwall;

  double* xsym;
  double* ysym;
  double* zsym;

  char* outname;

  int* colors;
  bool* type_active;
  char** type_name;
  char** sol_name;

  NodeSet    nodes;
  FaceMap    faces;
  BlockAlloc memfaces;
  BFaceSet   bfaces;
  ElementSet elements;
  int maxSurfID;
  double (*rotAxisPts)[6];

  bool IsPorous; // CBM

  int dim;
  double* res;

  int maxVolID;

  enum TYPE {UNDEFINED = -1, ICEM = 0, XPOST = 1, SINUS = 2, FLUID = 3} type;

  bool compute_nearest_normal;

public:

  Domain();
  ~Domain();

  static void print_help();

  virtual int get_arguments(int, char**);
  virtual int read() = 0;
  virtual int write() = 0;
  virtual void mirrors();
  virtual void check_topology();
  virtual void paint_bfaces();
  virtual void compute_distance();

  void mirror(int, double);
  void build_faces();
  void build_bfaces();
  void check_bfaces();
  void check_elements();
  void build_node2faces(int*, int**);
  void write_sinus();
  void write_xpost();
  void write_fieldview();
  void write_icem();

};

//------------------------------------------------------------------------------

#endif
