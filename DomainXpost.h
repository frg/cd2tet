#include <Domain.h>

//------------------------------------------------------------------------------

class DomainXpost : public Domain {

  char* meshname;

public:

  DomainXpost() { this->type = Domain::XPOST; }
  ~DomainXpost() {}

  int get_arguments(int, char**);
  int read();
  int write();

};

//------------------------------------------------------------------------------
