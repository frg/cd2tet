#include <Domain.h>

#include <stdio.h>
#include <stdlib.h>

#ifdef OLD_STL
#include <map.h>
#include <vector.h>
#include <string.h>
#else
#include <map>
#include <vector>
#include <string>
using std::map;
using std::vector;
using std::string;
#endif

extern int cmp_nocase(const string&, const string&, int = -1);

//------------------------------------------------------------------------------

struct SubDomainIcem {

  string name;
  int pid;
  int code;
  
  int nTria;
  int nTet;
  
  int* TriaElems;
  int* TetElems;
  
  string name1;
  string name2;
  int intdata1;
  double realdata1;
  double realdata2;
  double realdata3;
  double realdata4;
  double realdata5;
  double realdata6;
  double realdata7;
  double realdata8;
  double realdata9;

  SubDomainIcem() { 
    TriaElems = 0; 
    TetElems = 0; 
    nTria = 0;
    nTet = 0;
  }

  SubDomainIcem(string n, string n1, string n2 = "", int i1 = 0, double r1 = 0.0, 
	 double r2 = 0.0, double r3 = 0.0, double r4 = 0.0, double r5 = 0.0, 
	 double r6 = 0.0, double r7 = 0.0, double r8 = 0.0, double r9 = 0.0) { 
    name = n; pid = -1; code = 10; nTet = 0; 
    nTria = 0; TriaElems = 0; TetElems = 0; 
    name1 = n1; name2 = n2; intdata1 = i1; realdata1 = r1; 
    realdata2 = r2; realdata3 = r3; realdata4 = r4; realdata5 = r5; 
    realdata6 = r6; realdata7 = r7; realdata8 = r8; realdata9 = r9; 
    
    if (cmp_nocase(name1, string("INTERIOR")) == 0)
      code = 0;
    else if (cmp_nocase(name1, string("SYMMETRY"), 4) == 0)
      code = 6;
    else if (cmp_nocase(name1, string("PERIODIC"), 4) == 0)
      code = 7;
    else if (cmp_nocase(name1, string("SLIPWALL")) == 0)
      code = -2;
    else if (cmp_nocase(name1, string("SLIP")) == 0)
      code = -2;
    else if (cmp_nocase(name1, string("WALL")) == 0)
      code = -3;
    else if (cmp_nocase(name1, string("STICK")) == 0)
      code = -3;
    else if (cmp_nocase(name1, string("INLET"), 3) == 0)
      code = -4;
    else if (cmp_nocase(name1, string("OUTLET"), 3) == 0)
      code = -5;

    if (cmp_nocase(name2, string("FIXED")) == 0)
	code = abs(code);
  }
  ~SubDomainIcem() { 
     if (TriaElems) delete [] TriaElems;
     if (TetElems) delete [] TetElems;
  }
};

//------------------------------------------------------------------------------

typedef map<string, SubDomainIcem> IcemMap;

//------------------------------------------------------------------------------

class DomainIcem : public Domain {

  enum Format {XPOST = 0, FIELDVIEW = 1} format;
  char* domname;
  char* boconame;

public:

  DomainIcem() { this->type = Domain::ICEM; }
  ~DomainIcem() {}

  int get_arguments(int, char**);
  int read();
  int write();
  int read_boco(IcemMap&);
  int process_boco_line(FILE*, vector<string>&);
  int read_domain(IcemMap&);

};

//------------------------------------------------------------------------------
