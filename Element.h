#ifndef _ELEMENT_H_
#define _ELEMENT_H_

#include <Face.h>
#include <BlockAlloc.h>

struct Vec3D;
template<class Scalar, int dim> class SVec;

//------------------------------------------------------------------------------

namespace ElemDefs {

  enum Type {TET=5};
  
  static const int MaxNumNd = 8;
  static const int MaxNumEd = 12;
  static const int MaxNumFc = 6;
  
}

//------------------------------------------------------------------------------

class Element {

  int volume_id;

protected:

  static const double third;
  static const double sixth;
  static const double ninth;

public:

  // Number of nodes
  virtual int numNodes() = 0;

  // Number of edges
  virtual int numEdges() = 0;

  // Number of faces
  virtual int numFaces() = 0;

  virtual int* nodeNum() = 0;
  virtual int& nodeNum(int i) = 0;
  virtual const int faceDef(int i, int k) = 0;
  virtual const int faceNnd(int i) = 0;
  virtual const ElemDefs::Type type() = 0;
  virtual double compute_volume(SVec<double,3>&) = 0;
  virtual void swap() = 0;

  int &operator[](int i) { return nodeNum(i); }  

  // CBM: for porous media

  void setVolumeID(int i) { volume_id = i; }
  int& getVolumeID() { return volume_id; }
  void build_faces(FaceMap &fmap, BlockAlloc &facemem, int ele);

  int attachFace(int numNodesFace, int face[]);
};

//------------------------------------------------------------------------------

class ElemTet : public Element {

  static const int face_def[4][3];

  int nodes[4];

public:

  ElemTet() { setVolumeID(-1); }
  ~ElemTet() {}

  // Number of nodes
  int numNodes() { return 4; }

  // Number of edges
  int numEdges() { return 6; }

  // Number of faces
  int numFaces() { return 4; }

  int* nodeNum() { return nodes; }
  int& nodeNum(int i) { return nodes[i]; }
  const int  faceDef(int i, int k) { return face_def[i][k]; }
  const int  faceNnd(int i) { return 3; }
  const ElemDefs::Type type() { return ElemDefs::TET; }
  
  static double compute_volume(Vec3D x[4]);
  double compute_volume(SVec<double,3>&);

  void swap() {
    int nold[4] = {nodes[0], nodes[1], nodes[2], nodes[3]};
    
    nodes[0] = nold[1];
    nodes[1] = nold[0];
    nodes[2] = nold[2];
    nodes[3] = nold[3];
  }

};

//------------------------------------------------------------------------------

class ElementSet {

  int nelements;
  Element** elements;
  BlockAlloc memElems;

public:

  ElementSet();
  ~ElementSet();

  Element& operator[](int i) { return *elements[i]; }
  int size() { return nelements; }

  // Add pointer to Elem at position i in 
  void addElem(int i, Element *elem) { elements[i] = elem; }

  // Add pointer to Elem at position i in 
  Element& createElem(int i, const ElemDefs::Type type, int *nodes = 0) {

    // create elem in elemset depending on type
    switch (type) {
    case ElemDefs::TET:
      addElem(i, new(memElems) ElemTet);
      break;
      
    default:
      fprintf(stderr, "Error: Could not find elem type %d\n", type);
      exit(1);
    }

    if (nodes)
      for (int k=0; k<elements[i]->numNodes(); k++)
	elements[i]->nodeNum(k) = nodes[k];    

    return *elements[i];
  }

  int countElemType(const ElemDefs::Type type) {
    int count = 0;

    for (int i=0; i<nelements; i++)
      if (elements[i]->type()==type)
	count++;
    
    return count;
  }

  void resize(int);

};

//------------------------------------------------------------------------------

#endif
