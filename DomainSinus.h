#include <Domain.h>

//------------------------------------------------------------------------------

class DomainSinus : public Domain {

  char* meshname;

public:

  DomainSinus() { this->type = Domain::SINUS; }
  ~DomainSinus() {}

  int get_arguments(int, char**);
  int read();
  int write();

};

//------------------------------------------------------------------------------
