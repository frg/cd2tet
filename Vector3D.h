#ifndef _VECTOR3D_H_
#define _VECTOR3D_H_

#include <stdio.h>
#include <cmath>

//------------------------------------------------------------------------------

struct Vec3D {

  double v[3];

  Vec3D() {}
  Vec3D(double x[3]) { v[0] = x[0]; v[1] = x[1]; v[2] = x[2]; }
  Vec3D(double x, double y, double z) { v[0] = x; v[1] = y; v[2] = z; }
  Vec3D(const Vec3D &v2) { v[0] = v2.v[0]; v[1] = v2.v[1]; v[2] = v2.v[2]; }
  ~Vec3D() {}

  Vec3D &operator=(const double);
  Vec3D &operator=(const Vec3D &);
  Vec3D &operator+=(const Vec3D &);
  Vec3D &operator-=(const Vec3D &);
  Vec3D &operator*=(double);

  Vec3D operator+(const Vec3D &);
  Vec3D operator-(const Vec3D &);
  Vec3D operator^(const Vec3D &);

  double operator*(const Vec3D &);

  operator double*() { return v; }

  double &operator[](int i) { return v[i]; }
  double operator[](int i) const { return v[i]; }

  void print(char *msg = "") { fprintf(stderr, "%s(%e %e %e)\n", msg, v[0], v[1], v[2]); }

  double norm2();
  double norm();
  void normalize();

};

//------------------------------------------------------------------------------

inline 
Vec3D &Vec3D::operator=(const double v2)
{

  v[0] = v2;
  v[1] = v2;
  v[2] = v2;

  return *this;

}

//------------------------------------------------------------------------------

inline 
Vec3D &Vec3D::operator=(const Vec3D &v2)
{

  v[0] = v2.v[0];
  v[1] = v2.v[1];
  v[2] = v2.v[2];

  return *this;

}

//------------------------------------------------------------------------------

inline 
Vec3D &Vec3D::operator+=(const Vec3D &v2)
{

  v[0] += v2.v[0];
  v[1] += v2.v[1];
  v[2] += v2.v[2];

  return *this;

}

//------------------------------------------------------------------------------

inline 
Vec3D &Vec3D::operator-=(const Vec3D &v2)
{

  v[0] -= v2.v[0];
  v[1] -= v2.v[1];
  v[2] -= v2.v[2];

  return *this;

}

//------------------------------------------------------------------------------

inline 
Vec3D &Vec3D::operator*=(double cst)
{

  v[0] *= cst;
  v[1] *= cst;
  v[2] *= cst;

  return *this;

}

//------------------------------------------------------------------------------

inline 
Vec3D Vec3D::operator+(const Vec3D &v2)
{

  Vec3D res;

  res.v[0] = v[0]+v2.v[0];
  res.v[1] = v[1]+v2.v[1];
  res.v[2] = v[2]+v2.v[2];

  return res;

}

//------------------------------------------------------------------------------

inline 
Vec3D Vec3D::operator-(const Vec3D &v2)
{

  Vec3D res;

  res.v[0] = v[0]-v2.v[0];
  res.v[1] = v[1]-v2.v[1];
  res.v[2] = v[2]-v2.v[2];

  return res;

}

//------------------------------------------------------------------------------
// define vector cross product

inline 
Vec3D Vec3D::operator^(const Vec3D &v2)
{

  Vec3D res;

  res.v[0] = v[1]*v2.v[2]-v[2]*v2.v[1];
  res.v[1] = v[2]*v2.v[0]-v[0]*v2.v[2];
  res.v[2] = v[0]*v2.v[1]-v[1]*v2.v[0];

  return res;

}

//------------------------------------------------------------------------------

inline 
double Vec3D::operator*(const Vec3D &v2)
{

  return v[0]*v2.v[0] + v[1]*v2.v[1] + v[2]*v2.v[2];

}

//------------------------------------------------------------------------------

inline 
Vec3D operator*(double c, const Vec3D &v)
{

  Vec3D res;

  res.v[0] = c*v.v[0];
  res.v[1] = c*v.v[1];
  res.v[2] = c*v.v[2];

  return res;

}

//------------------------------------------------------------------------------

inline
double Vec3D::norm2()
{
  return (*this) * (*this);
}

//------------------------------------------------------------------------------

inline
double Vec3D::norm()
{
  return std::sqrt(this->norm2());
}

//------------------------------------------------------------------------------

inline
void Vec3D::normalize()
{
  double oonorm = 1. / this->norm();
  (*this) *= oonorm;
}

//------------------------------------------------------------------------------

#endif
