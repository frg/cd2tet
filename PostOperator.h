#ifndef _POST_OPERATOR_H_
#define _POST_OPERATOR_H_

//------------------------------------------------------------------------------

class PostOp {

public:

  enum TypeSol {ASCII = 0, DENS = 1, PRES = 2, TEMP = 3, KEN = 4, EPS = 5, 
		MACH = 6, VEL = 7, DISP = 8, SIZE = 9};

public:

  PostOp() {}
  ~PostOp() {}

  virtual void initialize(char *, char *, int = 0) = 0;

  virtual int getNumberOfNodes() = 0;
  virtual int getNumberOfElements() = 0;
  virtual int getNumberOfFaces() = 0;

  virtual double (*getNodes())[3] = 0;
  virtual int (*getElements())[4] = 0;
  virtual int (*getFaces())[4] = 0;

  virtual void releaseNodes() = 0;
  virtual void releaseElements() = 0;
  virtual void releaseFaces() = 0;

  virtual int getNumberOfSolutions(char *) = 0;
  virtual int getNumberOfNodalSolutions(char *) = 0;
  virtual bool getSolution(char *, int, TypeSol, double &, double *) = 0;

};

//------------------------------------------------------------------------------

#endif
