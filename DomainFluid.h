#include <Domain.h>

//------------------------------------------------------------------------------

class DomainFluid : public Domain {

  char* conname;
  char* meshname;
  char* resname;

public:

  DomainFluid() { this->type = Domain::FLUID; }
  ~DomainFluid() {}

  int get_arguments(int, char**);
  int read();
  int write();
  void mirrors() {}
  void check_topology() {}
  void paint_bfaces() {}
  void compute_distance() {}

};

//------------------------------------------------------------------------------
